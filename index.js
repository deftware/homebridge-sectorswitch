const fetch = require('node-fetch');
let Service, Characteristic;

module.exports = function (homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory("switch-plugin", "SectorSwitch", sectorSwitch);
};

sectorSwitch.prototype.getServices = function () {
    let informationService = new Service.AccessoryInformation();
    informationService
        .setCharacteristic(Characteristic.Manufacturer, "Deftware")
        .setCharacteristic(Characteristic.Model, "SmartPlug v1")
        .setCharacteristic(Characteristic.SerialNumber, "153-315-555");

    let switchService = new Service.Switch("SmartPlug");
    switchService
        .getCharacteristic(Characteristic.On)
        .on('get', this.getSwitchOnCharacteristic.bind(this))
        .on('set', this.setSwitchOnCharacteristic.bind(this));

    this.informationService = informationService;
    this.switchService = switchService;
    return [informationService, switchService];
}

sectorSwitch.prototype.setSwitchOnCharacteristic = function (on, next) {
    const action = on ? "Enable" : "Disable";
    fetch(`http://${this.sectorServer}:8085/SmartPlugs/${action}`).then(resp => resp.text()).then(data => {
        return next();
    });
}

sectorSwitch.prototype.getSwitchOnCharacteristic = function (next) {
    fetch(`http://${this.sectorServer}:8085/SmartPlugs/GetState`).then(resp => resp.json()).then(json => {
        return next(null, json[0].Status === "On");
    });
}

function sectorSwitch(log, config) {
    this.log = log;
    this.sectorServer = config['sectorServer'];
}
