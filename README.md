# SectorSwitch

A simple way to integrate Sector Alarm's smartplugs into Apple HomeKit using [HomeBridge](https://github.com/homebridge/homebridge/), powered by
[Sector Alarm API](https://gitlab.com/deftware/sectoralarmapi/).

# Prerequisites

This plugin relies on the [SectorAlarmAPI](https://gitlab.com/deftware/sectoralarmapi), head over there and follow the instructions for setting it up

# Install/Usage

1. Install with `sudo npm install -g homebridge-sectorswitch`
2. Add the following to the `accessories` array:

```json
{
    "accessory": "SectorSwitch",
    "name": "SmartPlug",
    "sectorServer": "<the ip address of your server running SectorAlarmAPI, without the port>"
}
```

The plugin will now work and a smartplug switch will appear in your Apple home app
